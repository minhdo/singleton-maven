package com.mark.Service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionService {
    private static Connection connection;

	static {
		
		try {
			ConfigurationService conService = new ConfigurationService();
			String driver = conService.getPropertyValue("spring.datasource.driverClassName");
			String username   = conService.getPropertyValue("spring.datasource.username");
			String password    = conService.getPropertyValue("spring.datasource.password");
			String url 		 = conService.getPropertyValue("spring.datasource.url");
			
			Class.forName(driver);
			connection = DriverManager.getConnection(url, username, password);

		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	public static Connection getConnection() {
		return connection;
	}
	
	
	

}
