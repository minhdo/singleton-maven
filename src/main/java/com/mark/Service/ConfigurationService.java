package com.mark.Service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class ConfigurationService implements Cloneable, Serializable {

	private static final long serialVersionUID = 1L;
	private static final String CONNECTION_FILE_WITH_PATH = "connection.properties";
	private static Properties properties = new Properties();

	static {
		InputStream file;

		try {
			file = ConfigurationService.class.getClassLoader().getResourceAsStream(CONNECTION_FILE_WITH_PATH);
			properties.load(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static Map<String, String> getProperties() {
		Map<String, String> propertyMap = new HashMap<String, String>();
		for (String propertyKey : properties.stringPropertyNames()) {
			propertyMap.put(propertyKey, properties.getProperty(propertyKey));
		}
		return propertyMap;
	}

	public static String getPropertyValue(String propertyKey) {
		if (propertyKey != null && !propertyKey.isEmpty()) {
			return properties.getProperty(propertyKey);
		}
		return null;
	}
}
