package com.mark.main;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.mark.Service.ConnectionService;

public class Application {

	private static String Query = "SELECT id, user_name, password, email, client_address FROM public.user_info;";
	public static void main(String[] args) {
		// MonoStage Pattern
		//ConfigurationService configurationService = new ConfigurationService();
		//Map<String, String> MapProperties =  configurationService.getProperties();
		
		/// Singleton Pattern
        try {
        	Connection conn = ConnectionService.getConnection();
        	Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(Query);
            
            while(rs.next()){
                System.out.print("ID: " + rs.getString("id"));
                System.out.print(", Age: " + rs.getString("user_name"));
                System.out.print(", First: " + rs.getString("password"));
                System.out.println(", Last: " + rs.getString("email"));
             }
        	
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
