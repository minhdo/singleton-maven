package com.mark.Interface;

import java.util.Map;

public interface ConnectionImpl {
	public Map<String, String> getProperties();
	public String getPropertyValue(String propertyKey);
}
